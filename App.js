import React from 'react';
import 'react-native-gesture-handler';
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux';
import CreateStore from './src/redux/store';
import Navigator from './src/navigator/Navigator';

const { store, persistor } = CreateStore();

const App = () => {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <Navigator/>
            </PersistGate>
        </Provider>
    );
};

export default App;
