import { createStore, applyMiddleware } from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist'
import reducers from './reducers';
import sagas from './sagas';


export default function CreateStore() {
    const persistConfig = {
        key: 'root',
        storage: AsyncStorage,
        whitelist: ['auth']
    };

    const sagaMiddleware = createSagaMiddleware();

    const persistedReducer = persistReducer(persistConfig, reducers);

    let store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
    let persistor = persistStore(store);
    store.rootTask = sagaMiddleware.run(sagas)
    return { store, persistor };
}
