import { combineReducers } from 'redux';
import auth from './modules/auth';
import allContacts from './modules/allContacts';
import recentContacts from './modules/recentContacts';


export default combineReducers({
    auth,
    allContacts,
    recentContacts
});
