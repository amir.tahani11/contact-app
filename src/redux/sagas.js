import { all, takeEvery } from 'redux-saga/effects';
import { LOGIN, watchLogin } from './modules/auth';
import { GET_ALL_CONTACTS, watchGetAllContacts } from './modules/allContacts';
import { GET_RECENT_CONTACTS, watchGetRecentContacts } from './modules/recentContacts';


export default function* rootTask() {
    yield all([
        takeEvery(LOGIN, watchLogin),
        takeEvery(GET_ALL_CONTACTS, watchGetAllContacts),
        takeEvery(GET_RECENT_CONTACTS, watchGetRecentContacts),
    ]);
}
