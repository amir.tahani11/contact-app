import { put } from 'redux-saga/effects';

export const GET_ALL_CONTACTS = 'contact/auth/GET_ALL_CONTACTS';
export const GET_ALL_CONTACTS_SUCCESS = 'contact/auth/GET_ALL_CONTACTS_SUCCESS';
export const GET_ALL_CONTACTS_FAILURE = 'contact/auth/GET_ALL_CONTACTS_FAILURE';


const initialState = {
    loading: false,
    loaded: false,
    allContacts: [],
    error: null,
};

export function getAllContacts() {
    return {
        type: GET_ALL_CONTACTS,
    };
}

function getAllContactsSuccess(allContacts) {
    return {
        type: GET_ALL_CONTACTS_SUCCESS,
        allContacts
    };
}

function getAllContactsFailure(error) {
    return {
        type: GET_ALL_CONTACTS_FAILURE,
        error
    };
}


export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case GET_ALL_CONTACTS:
            return {
                ...state,
                loading: true,
            };
        case GET_ALL_CONTACTS_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                allContacts: action.allContacts
            };
        case GET_ALL_CONTACTS_FAILURE:
            return {
                ...state,
                loading: false,
                loaded: true,
                error: action.error
            };
        default:
            return state;
    }
};


export function* watchGetAllContacts() {
    const allContactsMockData = [
        { id: 1, name: 'Mike', lastName: 'Portnoy' },
        { id: 2, name: 'Chris', lastName: 'Adler' },
        { id: 3, name: 'Rob', lastName: ' Halford' },
        { id: 4, name: 'Scott', lastName: 'Travis' },
        { id: 5, name: 'Dave', lastName: 'Lombardo' },
    ];
    try {
        return yield put(getAllContactsSuccess(allContactsMockData));
    } catch (e) {
        return yield put(getAllContactsFailure(e));
    }


}
