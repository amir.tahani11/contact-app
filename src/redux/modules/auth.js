import { put } from 'redux-saga/effects';

export const LOGIN = 'contact/auth/LOGIN';
export const LOGIN_SUCCESS = 'contact/auth/LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'contact/auth/LOGIN_FAILURE';
export const LOGOUT = 'contact/auth/LOGOUT';


const initialState = {
    loading: false,
    loaded: false,
    user: {},
    error: null,
    credentials: {}
};

export function login(credentials) {
    return {
        type: LOGIN,
        credentials
    };
}

function loginSuccess(user) {
    return {
        type: LOGIN_SUCCESS,
        user
    };
}

function loginFailure(error) {
    return {
        type: LOGIN_FAILURE,
        error
    };
}

export function logout() {
    return {
        type: LOGOUT
    };
}

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                loading: true,
                credentials: action.credentials
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                user: action.user
            };
        case LOGIN_FAILURE:
            return {
                ...state,
                loading: false,
                loaded: true,
                error: action.error
            };
        case LOGOUT:
            return initialState;
        default:
            return state;
    }
};


export function* watchLogin({ credentials }) {
    // we should send our real api request here
    if (credentials.email === 'amir@amir.com' && credentials.password === 'amir') {
        return yield put(loginSuccess({ name: 'Amir', lastName: 'Tahani' }));
    }
    return yield put(loginFailure({ message: 'user not found, please try again ' }));
}
