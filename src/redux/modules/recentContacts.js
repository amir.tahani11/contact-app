import { put } from 'redux-saga/effects';

export const GET_RECENT_CONTACTS = 'contact/auth/GET_RECENT_CONTACTS';
export const GET_RECENT_CONTACTS_SUCCESS = 'contact/auth/GET_RECENT_CONTACTS_SUCCESS';
export const GET_RECENT_CONTACTS_FAILURE = 'contact/auth/GET_RECENT_CONTACTS_FAILURE';


const initialState = {
    loading: false,
    loaded: false,
    recentContacts: [],
    error: null,
};

export function getRecentContacts() {
    return {
        type: GET_RECENT_CONTACTS,
    };
}

function getRecentContactsSuccess(recentContacts) {
    return {
        type: GET_RECENT_CONTACTS_SUCCESS,
        recentContacts
    };
}

function getRecentContactsFailure(error) {
    return {
        type: GET_RECENT_CONTACTS_FAILURE,
        error
    };
}


export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case GET_RECENT_CONTACTS:
            return {
                ...state,
                loading: true,
            };
        case GET_RECENT_CONTACTS_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                recentContacts : action.recentContacts
            };
        case GET_RECENT_CONTACTS_FAILURE:
            return {
                ...state,
                loading: false,
                loaded: true,
                error: action.error
            };
        default:
            return state;
    }
};


export function* watchGetRecentContacts() {
    const recentContactsMockData = [
        { id: 1, name: 'Jhon', lastName: 'Petrucci' },
        { id: 2, name: 'Dimebag', lastName: 'Darrel' },
        { id: 3, name: 'Joe', lastName: 'Bonamassa' },
        { id: 4, name: 'David', lastName: 'GilMour' },
        { id: 5, name: 'Joe', lastName: 'Satriani' },
        { id: 6, name: 'Kenneth', lastName: 'Downing' },
        { id: 7, name: 'Marty', lastName: 'Friedman' },
    ];
    try {
        return yield put(getRecentContactsSuccess(recentContactsMockData));
    } catch (e) {
        return yield put(getRecentContactsFailure(e));
    }


}
