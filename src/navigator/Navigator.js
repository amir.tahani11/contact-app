import React from 'react';
import { StyleSheet } from 'react-native';
import 'react-native-gesture-handler';
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import { Login, AllContacts, RecentContacts } from '../containers';
import DrawerContent from '../components/Navigator/DrawerContent';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();


const Navigator = () => {
    const user = useSelector(state => state.auth.user);

    const TabNavigator = () => {
        return (
            <Tab.Navigator tabBarOptions={{
                style: styles.tabBar,
                labelStyle: styles.tabBarText
            }}>
                <Tab.Screen name="AllContacts" component={AllContacts}/>
                <Tab.Screen name="RecentContacts" component={RecentContacts}/>
            </Tab.Navigator>
        );
    };

    function customDrawerContent(props) {
        return (
            <DrawerContent {...props}/>
        );
    }

    return (
        <NavigationContainer>
            {
                user.name ? <Drawer.Navigator drawerContent={customDrawerContent}>
                    <Drawer.Screen name="Home" component={TabNavigator}/>
                </Drawer.Navigator> : <Stack.Navigator screenOptions={{
                    headerShown: false
                }}>
                    <Stack.Screen name="Login" component={Login}/>
                </Stack.Navigator>
            }
        </NavigationContainer>
    );
};

const styles = StyleSheet.create({
    tabBar: {
        backgroundColor: '#7765E3',

    },
    tabBarText: {
        color: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        fontSize: 14,
        padding: 12
    }
});

export default Navigator;

