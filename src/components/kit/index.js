import Input from './Input/Input';
import Button from './Button/Button';
import Avatar from './Avatar/Avatar';
import Icon from './Icon/Icon';


export {
    Input,
    Icon,
    Button,
    Avatar
};
