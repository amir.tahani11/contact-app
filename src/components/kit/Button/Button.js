import React from 'react';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { Icon } from '../';

export default function Button(props) {
    return (
        <TouchableOpacity style={styles[`${props.type}`]} {...props}>
            <View style={styles.textWrapper}>
                <Icon name={props.iconName} size={props.iconSize}/>
                <Text style={styles[`${props.type}Text`]}>{props.children}</Text>
            </View>
        </TouchableOpacity>
    );
}


Button.defaultProps = {
    type: 'primary'
};

const styles = StyleSheet.create({
    textWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    primary: {
        backgroundColor: '#7765E3',
        paddingVertical: 8,
        paddingHorizontal: 20,
        width: 140,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8
    },
    primaryText: {
        justifyContent: 'center',
        color: '#fff'
    },
    secondaryText: {
        justifyContent: 'center',
        color: '#7765E3'
    }
});

