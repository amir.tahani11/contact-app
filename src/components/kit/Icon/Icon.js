import React from 'react';
import FeatherIcon from 'react-native-vector-icons/Feather';
import { StyleSheet } from 'react-native';


export default function Icon({ size, color, name, ...restProps }) {
    return (
        <FeatherIcon style={styles.icon} size={size} color={color} name={name} {...restProps}/>
    );
}

Icon.defaultProps = {
    color: '#7765E3',
    size: 20,
};


const styles = StyleSheet.create({
    icon: {
        marginRight: 4,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
