import React from 'react';
import { View, StyleSheet } from 'react-native';

export default function Avatar() {
    return (
        <View style={styles.avatar}/>
    );
}

const styles = StyleSheet.create({
    avatar: {
        backgroundColor: '#C4C4C4',
        borderRadius: 36,
        width: 72,
        height: 72,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20
    }
});
