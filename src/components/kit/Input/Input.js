import React from 'react';
import { TextInput, StyleSheet } from 'react-native';


export default function Input(props) {
    return (
        <TextInput
            style={styles.input}
            placeholderTextColor="#C4C4C4"
            placeholder={props.placeholder}
            {...props}
        />
    );
}


const styles = StyleSheet.create({
    input: {
        marginVertical: 12,
        height: 40,
        width: '100%',
        borderColor: '#000',
        borderWidth: 1,
        borderRadius: 8,
    }
});
