import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Text, View, StyleSheet } from 'react-native';
import { DrawerContentScrollView } from '@react-navigation/drawer';
import { logout } from '../../redux/modules/auth';
import { Avatar, Button } from '../kit';

export default function DrawerContent(props) {
    const user = useSelector(state => state.auth.user);
    const dispatch = useDispatch();
    return (
        <DrawerContentScrollView  {...props}>
            <View style={styles.wrapper}>
                <Avatar/>
                <View style={styles.usernameWrapper}>
                    <Text>{user.name}</Text>
                    <Text>{' '}</Text>
                    <Text>{user.lastName}</Text>
                </View>
            </View>
            <View style={styles.logout}>
                <Button onPress={() => dispatch(logout())} iconName="log-out" type="secondary">Logout</Button>
            </View>
        </DrawerContentScrollView>
    );
}


const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        alignItems: 'center',
        paddingVertical: 8,
        borderBottomWidth: 2,
        borderBottomColor: '#7765E3'
    },
    usernameWrapper: {
        flexDirection: 'row'
    },
    logout: {
        width: '100%',
        alignItems: 'flex-start',
        marginTop: 30,
        padding: 8
    }
});
