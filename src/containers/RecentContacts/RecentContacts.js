import React, { useEffect } from 'react';
import { FlatList, Text, View, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { getRecentContacts } from '../../redux/modules/recentContacts';

export default function RecentContacts() {
    const dispatch = useDispatch();
    const recentContacts = useSelector(state => state.recentContacts.recentContacts);
    useEffect(() => {
        dispatch(getRecentContacts());
    }, []);

    const renderItem = ({ item }) => (
        <View style={styles.itemContainer}>
            <View style={styles.itemWrapper}>
                <Text>{item.name}</Text>
                <Text>{' '}</Text>
                <Text>{item.lastName}</Text>
            </View>
        </View>
    );

    return (
        <>
            {
                recentContacts && recentContacts.length ? <FlatList
                    style={styles.list}
                    data={recentContacts}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                /> : null
            }

        </>
    );
}

const styles = StyleSheet.create({
    list: {
        paddingVertical: 24
    },
    itemWrapper: {
        flexDirection: 'row',
        paddingHorizontal: 12,
    },
    itemContainer: {
        paddingVertical: 12,

        borderBottomWidth: 2,
        borderBottomColor: '#7765E3'
    }
});
