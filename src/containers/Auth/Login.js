import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { View, Text, StyleSheet } from 'react-native';
import { login } from '../../redux/modules/auth';
import { Input, Button } from '../../components/kit';

export default function Login() {
    const dispatch = useDispatch();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = () => {
        dispatch(login({ email, password }));
    };

    return (
        <View style={styles.container}>
            <Text style={styles.text}>Contact App</Text>
            <Input
                onChangeText={setEmail}
                value={email}
                placeholder="Email"
            />
            <Input
                onChangeText={setPassword}
                value={password}
                placeholder="Password"
                secureTextEntry
            />
            <View style={styles.buttonWrapper}>
                <Button onPress={handleLogin}>
                    Login
                </Button>
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    buttonWrapper: {
        marginTop: 60
    },
    container: {
        padding: 16,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
    },
    text: {
        fontWeight: 'bold',
        fontSize: 24,
        marginBottom: 24
    }
});
