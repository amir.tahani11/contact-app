import React, { useEffect } from 'react';
import { FlatList, Text, View, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { getAllContacts } from '../../redux/modules/allContacts';

export default function AllContacts() {
    const dispatch = useDispatch();
    const allContacts = useSelector(state => state.allContacts.allContacts);
    console.log(allContacts)
    useEffect(() => {
        dispatch(getAllContacts());
    }, []);

    const renderItem = ({ item }) => (
        <View style={styles.itemContainer}>
            <View style={styles.itemWrapper}>
                <Text>{item.name}</Text>
                <Text>{' '}</Text>
                <Text>{item.lastName}</Text>
            </View>
        </View>
    );

    return (
        <>
            {
                allContacts && allContacts.length ? <FlatList
                    style={styles.list}
                    data={allContacts}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                /> : null
            }

        </>
    );
}

const styles = StyleSheet.create({
    list: {
        paddingVertical: 24
    },
    itemWrapper: {
        flexDirection: 'row',
        paddingHorizontal: 12,
    },
    itemContainer: {
        paddingVertical: 12,

        borderBottomWidth: 2,
        borderBottomColor: '#7765E3'
    }
});
