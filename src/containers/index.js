import Login from './Auth/Login';
import AllContacts from './AllContacts/AllContacts';
import RecentContacts from './RecentContacts/RecentContacts';

export {
    Login,
    RecentContacts,
    AllContacts
};
